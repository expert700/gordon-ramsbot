os.pullEvent = os.pullEventRaw
shell.run("menuApi")
os.loadAPI("menuApi")
os.loadAPI("gr/gr")
menu = {}
 
handle = assert(fs.open("gr/recipes", "r"), "Failed to open recipes")
output = handle.readAll()
recipes = textutils.unserialize(output)
handle:close()
 
table.insert(menu, {text="Update", method="shell.run('gr/installer')"})
table.insert(menu, {text="Add Recipe", method="shell.run('gr/addrecipe')"})
table.insert(menu, {text="Add Item", method="shell.run('gr/additem')"})
table.insert(menu, {text="Lua", method="shell.run('lua')"})

for k,v in pairs(recipes) do
    if not v[1].rHidden then
    table.insert(menu, {text=v[1].rName, method="gr.craft('"..v[1].rName.."') redstone.setOutput('right', true) sleep(2) redstone.setOutput('right', false)"})
    end
end

print("Press any key to continue...")
menuApi.runMenu(menu)