os.pullEvent = os.pullEventRaw
local sel = 1
local buttons = {}

local recipe = {}

table.insert(recipe, {rName="",rType=1})

for i = 1,9 do
  local slot = i
  if i > 3 and i < 7 then
    slot = i + 1
  elseif i > 6 then
    slot = i + 2
  end
  table.insert(recipe, {cType=0,cSlot=slot,cName=""})
end

table.insert(buttons, {x=1,y=1,width=3,height=2,slot=1,hasItem=false})
table.insert(buttons, {x=4,y=1,width=3,height=2,slot=2,hasItem=false})
table.insert(buttons, {x=7,y=1,width=3,height=2,slot=3,hasItem=false})
table.insert(buttons, {x=1,y=3,width=3,height=2,slot=4,hasItem=false})
table.insert(buttons, {x=4,y=3,width=3,height=2,slot=5,hasItem=false})
table.insert(buttons, {x=7,y=3,width=3,height=2,slot=6,hasItem=false})
table.insert(buttons, {x=1,y=5,width=3,height=2,slot=7,hasItem=false})
table.insert(buttons, {x=4,y=5,width=3,height=2,slot=8,hasItem=false})
table.insert(buttons, {x=7,y=5,width=3,height=2,slot=9,hasItem=false})
table.insert(buttons, {x=33,y=2,width=6,height=3,type="add",cType=1,text="Food"})
table.insert(buttons, {x=33,y=6,width=6,height=3,type="add",cType=2,text="Tool"})
table.insert(buttons, {x=33,y=10,width=6,height=3,type="add",cType=3,text="Item"})
table.insert(buttons, {x=26,y=6,width=6,height=3,type="finalize",text="Done"})

function clear()
  term.setBackgroundColor(colours.black)
  term.clear()
  term.setCursorPos(1,1)
end

function add( text , cType )
    draw()
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.white)
    term.setCursorPos(1,13)
    term.write("Enter name of "..text..": ")
    input = read()
    component = recipe[sel + 1]
    component.cType = cType
    component.cName=input
    buttons[sel].hasItem=true
end

function finalize()
  handle = fs.open("gr/recipes", "r")
  output = handle.readAll()
  handle:close()
  rTbl = textutils.unserialize(output)
  table.insert(rTbl, recipe)
  handle = fs.open("gr/recipes", "w")
  handle.write(textutils.serialize(rTbl))
  handle:close()
  return true
end

function drawBox(x,y,width,height,col,text)
  term.setCursorPos(x,y)
  for iy = 1,height do
    for ix = 1,width do
      paintutils.drawPixel(ix + x - 1,iy + y - 1, col)
    end
  end
  if text ~= nil then
    term.setCursorPos(math.ceil(x + (width/2) - (text:len() * 0.5)), math.floor(y + (height / 2)))
    term.setTextColor(colors.lightBlue)
    term.write(text)
  end
end

function draw()
  clear()
  for k,v in pairs(buttons) do
    local col = colors.white
    if v.hasItem then col = colors.green elseif v.slot == sel then col = colors.lightBlue end
    drawBox(v.x,v.y,v.width,v.height,col,v.text)
  end
end

function handleClick( but , x , y )
  for k,v in pairs(buttons) do
    if x >= v.x and x <= (v.x + v.width - 1) then
      if y >= v.y and y <= (v.y + v.height - 1) then
        if v.type == "add" then
          add(string.lower(v.text), v.cType)
        elseif v.type == "finalize" then
          turtle.place()
          local out = finalize()
          if out == true then
            return true 
          end
        else
          sel = v.slot
        end
      end
    end
  end
end

function handle()
  while true do
    local event, button, x, y = os.pullEventRaw("mouse_click")
    local out = handleClick(button, x, y)
    if out == true then break end
    draw()
  end
end

function init()
  term.clear()
  term.write("Name of recipe: ")
  local name = read()
  recipe[1].rName = name
  term.clear()
  print("1 - Normal Recipe")
  print("2 - Smelting Recipe")
  print("3 - Presser Recipe")
  print("Type of recipe: ")
  local rType = read()
  recipe[1].rType = tonumber(rType)
end

function main()
  init()
  draw()
  handle()
end

main()