os.pullEvent = os.pullEventRaw
os.loadAPI("/rom/apis/colors")
os.loadAPI("gr/gr")
_G.shell = shell
--[[ Variables ]]--
local offset = 0
local sel = 1
local printThanks = false
 
local bgColor, textColor, selectedBgColor, selectedTextColor
 
if term.isColor() then
 bgColor = colors.white
 textColor = colors.gray
 selectedBgColor = colors.lightBlue
 selectedTextColor = colors.white
else
 bgColor = colors.white
 textColor = colors.black
 selectedBgColor = colors.black
 selectedTextColor = colors.white
end
 
local maxX, maxY = term.getSize()
local running
 
--[[ Functions ]]--
 
clear = function()
 term.clear()
 term.setCursorPos(1,1)
end
 
local function centerWrite(txt)
 local x, y = term.getCursorPos()
 term.setCursorPos(math.floor(maxX/2-#tostring(txt)/2),y)
 write(txt)
end
 
local function redraw(tbl, sel, offset)
 term.setBackgroundColor(bgColor)
 clear()
 for i=1, maxY do
  if tbl[i] ~= nil then
   term.setCursorPos(1, i)
   if (i+offset) == sel then
    term.setBackgroundColor(selectedBgColor)
    term.clearLine()
    term.setTextColor(selectedTextColor)
    centerWrite("[ "..tostring(tbl[i + offset].text).." ]")
   else
    term.setBackgroundColor(bgColor)
    term.clearLine()
    term.setTextColor(textColor)
    centerWrite(tostring(tbl[i + offset].text))
   end
  end
 end
end
 
function runMenu( tbl )
 offset = 0
 sel = 1
 running = true
 while running do
  term.setCursorBlink(false)
  os.queueEvent("")
  os.pullEvent()
  redraw(tbl, sel, offset)
  local ev = {os.pullEvent()}
  if ev[1] == "key" then
   if ev[2] == keys.up then
    if sel > 1 then sel = sel - 1 end
    if offset > 0 then offset = offset - 1 end
   elseif ev[2] == keys.down then
    if sel < #tbl then sel = sel + 1 end
    if offset < math.max(#tbl - maxY, 0) then offset = offset + 1 end
   elseif ev[2] == keys.enter then
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.white)
    clear()
     if type(tbl[sel].text) == "string" then
      loadstring(tbl[sel].method)()
     end
   end
  elseif ev[1] == "mouse_scroll" then
   if ev[2] == -1 then
    if sel > 1 then sel = sel - 1 end
    if offset > 0 then offset = offset - 1 end
   elseif ev[2] == 1 then
    if sel < #tbl then sel = sel + 1 end
    if offset < math.max(#tbl - maxY, 0) then offset = offset + 1 end
   end
  elseif ev[1] == "mouse_click" then
   if tbl[(ev[4] + offset)] ~= nil then
    sel = ev[4] + offset
    redraw(tbl, sel, offset)
    sleep(.1)
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.white)
    clear()
   end
  end
 end
end
 
function stopMenu()
 running = false
 term.setBackgroundColor(colors.black)
 if printThanks == true then
  if term.isColor() == true then
   term.setTextColor(colors.yellow)
  end
  clear()
 else
  clear()
 end
end
 
function listMethods()
 local tmp = {}
 for i,v in pairs(menuApi) do
  table.insert(tmp, i.."()")
 end
 textutils.pagedTabulate(tmp)
 local tmp = nil
end
 
local function isColor(color)
 if term.isColor() then
  if type(color) == "string" then
   if colors[color] ~= nil then
    return {true, colors[color]}
   else
    return false
   end
  elseif type(color) == "number" then
   if color >= 1 and color <= colors.black then
    return {true, color}
   else
    return false
   end
  else
   return false
  end
 else
  return false
 end
end
 
function setBackgroundColor( color )
 local tmp = isColor(color)
 if tmp[1] then
  bgColor = tmp[2]
 end
end
 
function setBarColor( color )
 local tmp = isColor(color)
 if tmp[1] then
  selectedBgColor = tmp[2]
 end
end
 
function setTextColor( color )
 local tmp = isColor(color)
 if tmp[1] then
  textColor = tmp[2]
 end
end
 
function setBarTextColor( color )
 local tmp = isColor(color)
 if tmp[1] then
  selectedTextColor = tmp[2]
 end
end
 
setBarTextColor = setBarTextColor
setTextColor = setTextColor
setBarColor = setBarColor
setBackgroundColor = setBackgroundColor