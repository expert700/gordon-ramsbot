os.pullEvent = os.pullEventRaw
--[[ Directions

Forward - 1
Back - 4
Right - 2
Left - 3
Up - 5
Down - 6

]]--

--[[ Component Types

Food - 1
Tool - 2
Craftable - 3

]]--

--[[ Recipe Types

Normal - 1
Smelting - 2
Presser - 3

]]--
local craftables = {}
for i = 1,27 do
  table.insert(craftables,{taken=false,name="",slot=i,qty=0})
end
  
local state = {}
state.proto = {dir=1, x=0,y=0, c=craftables}
state.mt = {}
  
function state.new( vars )
  setmetatable(vars, state.mt)
  return vars
end
  
state.mt.__index = function (k, v)
  return state.proto[v]
end
  
local grState = state.new{dir=1, x=0,y=0, c=craftables}

function checkFuel()
  if turtle.getFuelLevel() == 0 then
    turtle.select(16)
    while turtle.getFuelLevel() < 100 do
      print("Please insert more fuel into the 16th slot")
      os.pullEvent("turtle_inventory")
      turtle.refuel()
    end
  end    
end


function face( sDir )
  if grState.dir == 2 then
    turtle.turnLeft()
    grState.dir = 1
  elseif grState.dir == 3 then
    turtle.turnRight()
    grState.dir = 1
  elseif grState.dir == 4 then
    turtle.turnRight()
    turtle.turnRight()
    grState.dir = 1
  end
  if sDir == 2 then
    turtle.turnRight()
    grState.dir = 2
  elseif sDir == 3 then
    turtle.turnLeft()
    grState.dir = 3
  elseif sDir == 4 then
    turtle.turnLeft()
    turtle.turnLeft()
    grState.dir = 4
  end
end

function collide( sSucc, sDir)
  checkFuel()
  while sSucc == false do
    sleep(1)
    if sDir == 1 then
      sSucc = turtle.forward()
    elseif sDir == 4 then
      sSucc = turtle.back()
    elseif sDir == 5 then
      sSucc = turtle.up()
    elseif sDir == 6 then
      sSucc = turtle.down()
    end
  end
end

function go( sX , sY )
sX = tonumber(sX)
sY = tonumber(sY)
-- X Movement
  if sX > grState.x then
    local xDest = sX - grState.x
    face(1)
    for i = 1,xDest do
      local succ = turtle.forward()
      collide(succ, 1)
      grState.x = grState.x + 1
    end
  end
 
  if grState.x > sX then
    local xDest = grState.x - sX
    face(1)
    for i = 1,xDest do
      local succ = turtle.back()
      collide(succ, 4)
      grState.x = grState.x - 1
    end
  end
 
-- Y Movement
  if sY > grState.y then
    local yDest = sY - grState.y
    face(1)
    for i = 1,yDest do
      local succ = turtle.up()
      collide(succ, 5)
      grState.y = grState.y + 1
    end
  end
 
  if grState.y > sY then
    local yDest = grState.y - sY
    face(1)
    for i = 1,yDest do
      local succ = turtle.down()
      collide(succ, 6)
      grState.y = grState.y - 1
    end
  end
end
 
function item( name, iType, slot, qty, ret)
  qty = qty or 1
  ret = ret or false
  local iHandle = fs.open("gr/items", "r")
  local iTable = textutils.unserialize(iHandle.readAll())
  iHandle:close()
  local iFound = {}
  for k,v in pairs(iTable) do
    if v.name == name and v.iType == iType then
      iFound = v
    end
  end
  if iFound.x == nil or iFound.y == nil then error("Nil coordinates") end
  go(iFound.x,iFound.y)
  if iType == 1 then face(3) elseif iType == 2 then face(2) end
  turtle.select(slot)
  turtle.suck(qty)
  if ret == true then turtle.drop(qty) end
  face(1)
end

function checkInv()
  local p = peripheral.wrap("bottom")
  while p == nil do
    sleep(0.1)
    p = peripheral.wrap("bottom")
  end
  for i = 1,27 do
    local stack = p.getStackInSlot(i)
    if stack ~= nil then
      grState.c[i].taken = true
      grState.c[i].name = stack.display_name
      grState.c[i].slot = i
      grState.c[i].qty = stack.qty
    else
      grState.c[i].taken = false
    end
  end
end
 
function craft( cName , cQty )
  checkInv()
  
  cQty = cQty or 1
 
  local cHandle = fs.open("/gr/recipes","r")
  local cTable = textutils.unserialize(cHandle.readAll())
  cHandle:close()
  local cRecipe = {}
  for k,v in pairs(cTable) do
    if v[1].rName == cName then
      cRecipe = v
    end
  end
 
  print(cRecipe[1].rName)
 
  for k,v in pairs(cRecipe) do
    if v.cType == 3 then
      local finding = true
      craft(v.cName, cQty)
      for kC, vC in pairs(grState.c) do
        if vC.taken == true and finding == true and vC.name == v.cName and vC.qty < 64 then
          vC.qty = vC.qty + cQty
          print("Added " ..cQty.. " of: "..vC.name.." to craftables.")
          print("In Slot:"..vC.slot)
          finding = false
        elseif vC.taken == false and finding == true then
          vC.taken = true
          vC.name = v.cName
          vC.qty = cQty
          print("Added: "..vC.name.." to craftables.")
          print("In Slot:"..vC.slot)
          finding = false
        end
      end
    end
  end
 
  for k,v in pairs(grState.c) do
    if v.taken == true then
      print("Name: "..v.name.." Slot: "..v.slot.." Quantity: "..v.qty)
    end
  end
  toget = {}
  for k,v in pairs(cRecipe) do
    if v.cType == 3 then
      print("Getting: "..v.cName.." for slot "..v.cSlot)
      usecSlot = v.cSlot
      for kC,vC in pairs(grState.c) do
        if v.cName == vC.name then
          print("Retrieving component: "..v.cName)
          go(0,0)
          checkInv()
          turtle.select(usecSlot)
          p = peripheral.wrap("bottom")
          p.pushItem("UP", vC.slot, cQty, turtle.getSelectedSlot())
          if vC.qty - cQty >= 1 then
            vC.qty = vC.qty - cQty
          else
            vC.taken = false
            vC.name = ""
            vC.qty = 0
          end
        end
      end
    elseif v.cType == 1 or v.cType == 2 then
      local iQty = 1
      if v.cType == 1 then iQty = cQty end
      item(v.cName, v.cType, v.cSlot, iQty, false)
    end      
  end
 
  -- Travel to above output chest
 
  turtle.select(16)
  go(0,0)
  checkInv()
  face(1)
  if tonumber(cRecipe[1].rType) == 2 then
    print("Type = 2")
    go(-1,0)
    turtle.select(1)
    turtle.dropDown(cQty)
    go(0,0)
    checkInv()
    face(1)
    sleep(6 * cQty)
  elseif tonumber(cRecipe[1].rType) == 3 then
  print("Type = 3")
    go(-2,0)
    turtle.select(1)
    turtle.dropDown(cQty)
    go(0,0)
    checkInv()
    face(1)
    sleep(6.5 * cQty)
  elseif cRecipe[1].rType == nil or 1 then
    print("Type = 1")
    print("Crafting")
    for i = 1,cQty do
      turtle.craft()
      turtle.dropDown(cQty)
    end
  end
 
  for k,v in pairs(cRecipe) do
    if v.cType == 2 then
      item(v.cName, 2, v.cSlot, 1, true)
    end
  end
  go(0,0)
  checkInv()
  face(1)
end